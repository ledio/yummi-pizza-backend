<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Customer;
use App\Order;
use App\OrderProduct;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'customer.full_name' => 'required',
            'customer.email' => 'required|email',
            'customer.cell' => 'required',
            'customer.address' => 'required',
            'products.*.id' => 'required|integer',
            'products.*.quantity' => 'required|numeric|not_in:0',
        ]);

        $cart = Cart::get($request);

        $customer = Customer::create($request->get('customer'));

        $order = Order::create([
            'customer_id' => $customer->id,
            'currency' => $cart['currency'],
            'total_amount' => $cart['total'],
        ]);

        $orderProducts = [];
        foreach ($cart['products'] as $product) {
            $orderProducts[] = new OrderProduct([
                'order_id' => $order->id,
                'name' => $product['name'],
                'quantity' => $product['quantity'],
                'price' => $product[$cart['currency'] == 'euro' ? 'price_euro' : 'price_dollar'],
            ]);
        }

        $order->products()->saveMany($orderProducts);

        return response()->json([
            'title' => 'Your order is made successfully',
        ]);
    }
}
