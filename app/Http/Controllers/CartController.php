<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'products.*.id' => 'required|integer',
            'products.*.quantity' => 'required|numeric|not_in:0',
        ]);

        return response()->json(Cart::get($request));
    }
}
