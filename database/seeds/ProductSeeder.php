<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        Product::insert([
            ['name' => 'MARGHERITA', 'description' => 'Traditional tomato and mozzarella cheese with Italian spices.', 'price' => 14, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
            ['name' => 'VEGETARIAN', 'description' => 'Diced tomatoes, onions, assorted peppers, mushrooms and pineapple.', 'price' => 16, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
            ['name' => 'MEDITERRANEAN', 'description' => 'Black olives, Peppers, sun-dried tomatoes and feta.', 'price' => 18, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
            ['name' => 'HAWAIIAN', 'description' => 'Chicken strips and pineapple.', 'price' => 16, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
            ['name' => 'CALIFORNIAN', 'description' => 'Chicken strips, feta and avocado (seasonal).', 'price' => 18, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
            ['name' => 'CHICKEN', 'description' => 'Chicken, mayonnaise and spring onions.', 'price' => 18, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
            ['name' => 'BBQ CHICKEN', 'description' => 'Onions, tomatoes, roast chicken, mushrooms and BBQ sauce.', 'price' => 18, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
            ['name' => 'SANTE FÉ', 'description' => 'Spicy beef sausage, beef salami, mushrooms and pineapple.', 'price' => 22, 'image' => 'https://i.ibb.co/2nTz9jL/pizza.jpg'],
        ]);
    }
}
