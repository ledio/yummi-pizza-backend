<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\JsonResponse;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function __invoke()
    {
        return response()->json(Product::all());
    }
}
