<?php


namespace App;


use Illuminate\Http\Request;

class Cart
{
    public $request;

    public $products = [];

    public $currency = 'euro';

    public $subtotal = 0;
    public $total = 0;
    public $deliveryFee = 0;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this
            ->setCurrency()
            ->setDeliveryFee()
            ->setProducts()
            ->calculate();
    }

    public function setCurrency()
    {
        if ($this->request->has('currency')) {
            $this->currency = $this->request->get('currency');
        }

        return $this;
    }

    public function setProducts()
    {
        $products = collect($this->request->get('products'));

        $this->products = Product::findOrFail($products->pluck('id'))->map(function ($product) use ($products) {

            return [
                'id' => $product->id,
                'name' => $product->name,
                'quantity' => $products->firstWhere('id', $product->id)['quantity'],
                'price_euro' => $product->price_euro,
                'price_dollar' => $product->price_dollar,
            ];

        })->toArray();

        return $this;
    }

    public function setDeliveryFee()
    {
        $this->deliveryFee = round(env('DELIVERY_FEE'),2);

        return $this;
    }

    public function calculate()
    {
        foreach ($this->products as $product) {
            $this->subtotal += ($product[$this->getPriceKey()] * $product['quantity']);
        }

        $this->subtotal = round($this->subtotal, 2);
        $this->total = round($this->subtotal + $this->deliveryFee, 2);

        return $this;
    }

    public function getPriceKey()
    {
        return $this->currency == 'euro' ? 'price_euro' : 'price_dollar';
    }

    public function result()
    {
        return [
            'products' => $this->products,
            'currency' => $this->currency,
            'subtotal' => $this->subtotal,
            'total' => $this->total,
            'delivery_fee' => $this->deliveryFee,
        ];
    }

    public static function get(Request $request)
    {
        return (new static($request))->result();
    }

}
