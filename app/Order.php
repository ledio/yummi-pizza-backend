<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const CURRENCY_EURO = 'euro';
    const CURRENCY_DOLLAR = 'dollar';

    protected $fillable = ['customer_id', 'total_amount', 'currency'];

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }
}
