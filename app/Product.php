<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'price'];

    protected $hidden = ['price'];

    protected $appends = ['price_euro', 'price_dollar'];

    public function getPriceEuroAttribute()
    {
        return round($this->attributes['price'], 2);
    }

    public function getPriceDollarAttribute()
    {
        return round($this->attributes['price'] * env('CONVERSION_RATE', 1), 2);
    }
}
